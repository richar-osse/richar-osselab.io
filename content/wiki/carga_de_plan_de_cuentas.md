Aquí iniciaremos definiendo las cuentas y tipos de cuentas establecidos en PLAN CONTABLE GENERAL EMPRESARIAL emitido por el Ministerio de Economía y Finanzas, cargamos el plan contable bajo la estructura XML, previamente es importante conocer la relación de los modelos que intervienen en carga.
1.  account_chart_template: Plantilla para la definición del Plan Contable.
2.  account_account_template: Plantilla donde será cargadas las cuentas del plan Contable mediante los XML de data, está disponible para ser copiado al plan de una o varias empresa, más no nos utilizadas en los procesos contables de las mismas.
3.  account_account: Mediante la ejecución del yml copia el account_account_template para obtener la data del plan contable, este modelo es el utilizado en los procesos de la empresa.
4.  account_account_type: Clasifica las cuentas dependiendo de los criterios del cliente, es esta clasificación es la plasmada en los estados financieros.
![screenshot-localhost_8120-2019-02-20-11-06-24](uploads/97cf1469c0174d186ba44fe37c02d275/screenshot-localhost_8120-2019-02-20-11-06-24.png)
