{ \
echo "source 'https://rubygems.org'"; \
echo "gem 'nanoc', '~> 4.10'"; \
echo "gem 'adsf', '~> 1.4'"; \
echo "gem 'adsf-live', '~> 1.4'"; \
echo "gem 'sassc', '~> 2.0'"; \
echo "gem 'rouge', '~> 3.2'"; \
echo "gem 'rake', '~> 12.3'"; \
echo "gem 'colorize', '~> 0.8.1'"; \
echo "group :nanoc do"; \
echo    "gem 'guard-nanoc', '~> 2.1'"; \
echo    "gem 'gitlab_kramdown', '~> 0.4.2'"; \
echo    "gem 'nokogiri', '~> 1.7.0'"; \
echo    "gem 'builder', '~> 3.2'"; \
echo "end"; \
echo "group :test, :development do"; \
echo    "gem 'scss_lint', '~> 0.57', require: false"; \
echo    "gem 'highline', '~> 2.0'"; \
echo    "gem 'rspec', '~> 3.5'"; \
echo "end"; \
} >> Gemfile
