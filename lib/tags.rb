#def tags
  #if @item[:tags].nil?
    #'(none)'
  #else
    #@item[:tags].join(', ')
  #end
#end


use_helper Nanoc::Helpers::Tagging
use_helper Nanoc::Helpers::Rendering
use_helper Nanoc::Helpers::XMLSitemap
