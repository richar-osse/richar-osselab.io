require 'yaml'
#require 'colorize'

#PRODUCTS = %W[osse docunexo oxe extra oxe360].freeze
PRODUCTS = %W[osse docunexo oxe360].freeze
VERSION_FORMAT = /^(?<major>\d{1,2})\.(?<minor>\d{1,2})$/

def config
  @config ||= YAML.load_file('./nanoc.yaml')
end

def products
  return @products if defined?(@products)

  @products = PRODUCTS.each_with_object({}) do |key, result|
     result[key] = config['products'][key]
  end
end

def retrieve_branch(slug)
    if PRODUCTS.include? slug
        slug
    elsif ENV["CI_COMMIT_REF_NAME"].nil?
        'master'
    elsif version = ENV["CI_COMMIT_REF_NAME"].match(VERSION_FORMAT)
        case slug
        when 'ee'
            "#{version[:major]}-#{version[:minor]}-stable-ee"
        when 'ce', 'omnibus', 'runner'
            "#{version[:major]}-#{version[:minor]}-stable"
        else
            'master'
        end
    else
        ENV.fetch("BRANCH_#{slug.upcase}", 'master')
    end
end

def git_workdir_dirty?
  status = `git status --porcelain`
  !status.empty?
end

def chart_version(gitlab_version)
  config = YAML.load_file('./content/_data/chart_versions.yaml')
  config.fetch(gitlab_version)
end
